.size 0x68
.section #payload 0x1100
.b32 0x5b4 // Gadget that sets $r10 to 0
.b32 0x647 // gen_usr_key function, needs $r10 to be 0, needs $r11 to be != 0
.b32 0x5b8 // Gadget that sets $r10 to 1
.b32 0x5bd // crypto_load function, takes keyslot in first 3 bits of $r10, takes 0x10-Aligned output buffer in $r11
.b32 0x3be // Bring memory out of lockdown
.b32 0xf5e // Address for success in boot.asm
