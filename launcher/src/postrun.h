/*
* Copyright (c) 2022 EliseZeroTwo <mail@elise.moe>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms and conditions of the GNU General Public License,
* version 2, as published by the Free Software Foundation.
*
* This program is distributed in the hope it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _POSTRUN_H_
#define _POSTRUN_H_

#include "hwinit.h"
#include "fs.h"

const unsigned int TSEC_ENTRYPOINT = 0x00;

void postrun(tsec_res_t* run) {
    gfx_printf("MBX0: %x\nMBX1: %x\nEXCI: %x\nSEC err: %x\nTrace PC: %x\nStat0: %x\nStat1: %x\nStat2: %x\n", run->mbx0, run->mbx1, run->exci, run->sec_err, run->trace_pc, run->scp_stat0, run->scp_stat1, run->scp_stat2);

    if (!sdMount()) {
        print("No SD card, can't write files\n");
    } else {
        if (!fopen("/dmem.bin", "wb")) {
            print("Failed to open dmem.bin for writing\n");
        } else {
            if (!fwrite(run->dmem, 0x4000, 1)) {
                print("Failed to write dmem.bin\n");
            }
            fclose();
        }

        if (!fopen("/imem.bin", "wb")) {
            print("Failed to open imem.bin for writing\n");
        } else {
            if (!fwrite(run->imem, 0x1000, 1)) {
                print("Failed to write imem.bin\n");
            }
            fclose();
        }
    }
}

#endif