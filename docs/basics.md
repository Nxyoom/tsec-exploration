# Basics

## Firmware Stages

The firmware that runs on the TSEC is split up into multiple stages. Before HOS version 6.2.0 there were only three stages, the first unsigned and stored decrypted, the second signed and stored decrypted, and the third signed and encrypted. The second stage decrypts the third stage manually rather than using the encryption features offered by the ROM that validates the signature when entering a new secure stage.

These stages are called:

1. Boot
2. KeygenLDR
3. Keygen

The stages will return to the previous stage upon success.

## Memory

There are two seperate types of memory:

* Code Memory (0x8000 bytes in the Switch but can vary on other devices)
* Data Memory (0x4000 bytes)

Code memory is virtual and cannot be interacted with via regular read/write operations and to interact with it you must either use an MMIO window or DMA.

Addresses wrap around so writing to 0x4001 will write to 0x1.

Pages are 0x100 bytes long.

Code pages have attributes that mark if they are secret and authenticated. You can set a page as secret but not as authenticated. Attempting to read from a page marked as secret will return the constant 0xDEAD5EC1. Attempting to execute from a page that is marked as secret and not as authenticated will result in a secure fault being raised which causes a ROM to authenticate the pages marked as secret by calculating a signature over them and comparing it to one you provide, marking them as authenticated on succcess and restarting execution from the start of the authenticated pages otherwise it halts on failure. Pages automatically loose the authentication flag once the PC points outside of the section of authenticated pages.

## Security Mode

The secure coprocessor can run in three different security modes:

* No Secure (NS)
* Low Secure (LS)
* Heavy Secure (HS)

NS is the default mode and does not require any authentication. To enter HS mode you need to enter into code marked as secure as discussed above in the `Memory` section. In HS mode you have access to more crypto instructions and more hardware secrets than in other modes. LS mode can only be entered from HS mode and is mostly irrelvant to us currently.

## Hardware Secrets

There are 64 hardware secrets with varying access control levels.
