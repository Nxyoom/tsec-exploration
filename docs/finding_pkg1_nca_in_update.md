# Finding the NCA containing Package1 in a Firmware Update

Here is a bad shell script that finds the NCA containing Package1 in section 0 in a firmware update. If it returns a `.cnmt.nca` just temporarily move that NCA or make the name no longer end in `.nca` (I said it was bad).

```sh
for FILE in *.nca; do;                                                                                                                                                                                                                                                                                             
        if hactool -t nca $FILE 2>/dev/null | grep -q "Title ID:                           0100000000000819"; then
                echo "Found $FILE"
                break
        fi
done
```
