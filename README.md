# TSEC Exploration

A collection of things I built to run on the Switch whilst exploring the TSEC.

## Currently Here

* A launcher for easily testing TSEC firmwares
* A ROP chain to steal the encryption key for the keygen blob
* A ROP chain to steal the signing key for the boot blob
* A ROP chain to generate a fakesigning key
* An exploit chain to obtain heavy-secure readable hardware secrets 

## Documentation

* [Basics](./docs/basics.md)
* [Guide to decrypting Keygen](./docs/decrypting_keygen.md)
* [Explanation of Keygen decryption](./docs/decrypting_keygen_explanation.md)

## Related Tools

* [nx-tsec-append-blob](https://gitlab.com/elise/nx-tsec-append-blob)
* [nx-tsec-firmware-splitter](https://gitlab.com/elise/nx-tsec-firmware-splitter)
* [nx-tsec-decrypt-keygen](https://gitlab.com/elise/nx-tsec-decrypt-keygen)

## License

Unless otherwise specified everything in this repository is under the MIT license
