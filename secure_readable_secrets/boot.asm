.section #payload 0xF00
_start:
    mov $r10 0x1250
    mov $sp $r10

    mov $r10 0x1c00 // dst
    mov $r11 0x2000 // src
    mov $r12 0x10 // len
    lcall #memcpy_i2d

    mov $r10 6
    mov $r11 0x1c00
    lcall #crypto_store
    
    clear b32 $r10 // dst
    mov $r11 0x1300 // src
    mov $r12 0xd00 // len
    lcall #memcpy_i2d

    mov $r10 0x5f00 // dst
    clear b32 $r11 // src
    mov $r12 0xd00 // len
    mov $r13 0x1 // is_secret
    lcall #memcpy_d2i

    mov $r10 0xc00
    mov $r11 0xF800F8
    mov $r12 0x100
    lcall #memset

    mov $r10 0xb00
    mov $r11 0x0
    mov $r12 0x100
    lcall #memset

    mov $r10 0xc00 // dst
    mov $r11 0xc00 // src
    mov $r12 0x100 // len
    mov $r13 0x0 // is_secret
    lcall #memcpy_d2i

    mov $r9 0xd00
    shl b32 $r9 0x10
    or $r9 0x5F
    mov $cauth $r9

secret0:
    mov $r10 #secret5
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2000
    bra 0x5f00

secret5:
    mov $r10 #secret9
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 5
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2050
    bra 0x5f00

secret9:
    mov $r10 #secretc
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 9
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2090
    bra 0x5f00

secretc:
    mov $r10 #secretf
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0xc
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x20c0
    bra 0x5f00

secretf:
    mov $r10 #secret15
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0xf
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x20f0
    bra 0x5f00

secret15:
    mov $r10 #secret18
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x15
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2150
    bra 0x5f00

secret18:
    mov $r10 #secret1b
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x18
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2180
    bra 0x5f00

secret1b:
    mov $r10 #secret1e
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x1b
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x21b0
    bra 0x5f00

secret1e:
    mov $r10 #secret21
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x1e
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x21e0
    bra 0x5f00

secret21:
    mov $r10 #secret24
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x21
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2210
    bra 0x5f00

secret24:
    mov $r10 #secret27
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x24
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2240
    bra 0x5f00

secret27:
    mov $r10 #secret2a
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x27
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2270
    bra 0x5f00

secret2a:
    mov $r10 #secret2d
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x2a
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x22a0
    bra 0x5f00

secret2d:
    mov $r10 #secret30
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x2d
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x22d0
    bra 0x5f00

secret30:
    mov $r10 #secret33
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x30
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2300
    bra 0x5f00

secret33:
    mov $r10 #secret36
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x33
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2330
    bra 0x5f00

secret36:
    mov $r10 #secret39
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x36
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2360
    bra 0x5f00

secret39:
    mov $r10 #secret3c
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x39
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x2390
    bra 0x5f00

secret3c:
    mov $r10 #continue
    push $r10
    mov $r10 0x63d2
    push $r10
    csecret $c2 0x3c
    mov $r12 0x10
    mov $r14 0x100
    mov $r0 0xb00
    mov $r1 0x23c0
    bra 0x5f00
    
continue:
    add $sp -0x80 // Allocate space on the stack for the key_buffer
    mov $r5 $sp // key_buffer

    mov $r10 0xFFAABBFF
    lcall #write_mbox1

    mov b32 $r10 $r5 // dst
    mov $r11 0x300 // src
    mov $r12 0x7C // len
    lcall #memcpy_i2d

    clear b32 $r10 // dst
    mov $r11 0x400 // src
    ld b32 $r12 D[$r5 + 0x74] // len
    lcall #memcpy_i2d

    mov $r10 0x300 // dst
    clear b32 $r11 // src
    ld b32 $r12 D[$r5 + 0x74] // size
    mov $r13 0x1 // is_secret
    lcall #memcpy_d2i

    ld b32 $r9 D[$r5 + 0x74] // blob1_size
    shl b32 $r9 0x10
    or $r9 0x3
    mov $cauth $r9

    mov $r10 6
    mov b32 $r11 $r5
    add b32 $r11 0x20
    lcall #crypto_store

    mov $r8 #success
    // Keygenldr args
    mov b32 $r10 $r5
    mov $r11 0x0 // Skip calling keygen
    mov $r12 0x1 // Skip decryption of keygen

    // Keygenldr
    lcall 0x300
success:
    lcall #write_mbox0

    mov $r10 0xF100F
    lcall #write_mbox1
exit:
    exit
    bra b #exit

/*
    r10 - reg
    r11 - buffer
*/
crypto_store:
    shl b32 $r10 0x10 
    or $r10 $r11
    cxset 0x2
    xdst $r10 $r10
    xdwait
    ret

/*
    r10 - reg
    r11 - buffer
*/
crypto_load:
    shl b32 $r10 0x10 
    or $r10 $r11
    cxset 0x2
    xdld $r10 $r10
    xdwait
    ret

/*
    r10 - dst
    r11 - val
    r12 - size
*/
memset:
    bra #memset_check
memset_loop:
    st b32 D[$r10] $r11
    add b32 $r10 4
    sub b32 $r12 4
memset_check:
    bra b32 $r12 0 ne #memset_loop
    ret

/*
    r10 - dest
    r11 - src
    r12 - size

    pollutes: r9
*/
memcpy:
    bra #mcpy_check
mcpy_loop:
    ld b32 $r9 D[$r11]
    st b32 D[$r10] $r9
    add b32 $r10 4
    add b32 $r11 4
    sub b32 $r12 4
mcpy_check:
    bra b32 $r12 0 ne #mcpy_loop
    ret


/* 
    r10 - dest
    r11 - src
    r12 - size
*/
memcpy_i2d:
    mov $r9 0x2000000
    or $r11 $r11 $r9
    mov $r8 0x6000
    mov $r9 0x6100
    iowr I[$r8] $r11
memcpy_i2d_loop:
    iord $r15 I[$r9]
    st b32 D[$r10] $r15
    sub b32 $r12 4
    add b32 $r10 4
memcpy_i2d_check_remaining:
    cmp b32 $r12 0
    bra ne #memcpy_i2d_loop
    ret

/* 
    r10 - dest
    r11 - src
    r12 - size
    r13 - secret
*/
memcpy_d2i:
    mov $r9 0x1000000
    bra b8 $r11 0x0 ne #md2i_invalid_param
    bra b8 $r12 0x0 ne #md2i_invalid_param
    cmp b32 $r13 0
    bra e #md2i_not_secret
    mov $r9 0x11000000
md2i_not_secret:
    or $r9 $r10 $r9
    mov $r15 0x6000
    iowr I[$r15] $r9
    mov $r8 0x6100
    mov $r9 0x6200
md2i_write_loop_check_new_vpage:
    and       $r7 $r10 0xff
    bra       z #md2i_write_loop_new_vpage
md2i_write_loop:
    ld b32    $r1 D[$r11]
    ld b32    $r2 D[$r11 + 0x4]
    ld b32    $r3 D[$r11 + 0x8]
    ld b32    $r4 D[$r11 + 0xc]
    iowr      I[$r8] $r1
    iowr      I[$r8] $r2
    iowr      I[$r8] $r3
    iowr      I[$r8] $r4
    add b32   $r10 0x10
    add b32   $r11 0x10
    sub b32   $r12 0x10
md2i_check_remaining:
    cmp b32 $r12 0x0
    bra ne #md2i_write_loop_check_new_vpage
    ret
md2i_write_loop_new_vpage:
    shr b32   $r7 $r10 0x8
    iowr      I[$r9] $r7
    bra      #md2i_write_loop
md2i_invalid_param:
    bra #md2i_invalid_param

/* 
    r10 - value
*/
write_mbox0:
    mov $r1 0x1000
    iowr I[$r1] $r10
    ret

/* 
    r10 - value
*/
write_mbox1:
    mov $r1 0x1100
    iowr I[$r1] $r10
    ret


exit
exit
exit
exit
